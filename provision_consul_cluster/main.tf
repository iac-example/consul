###############################################################################
# Inputs
###############################################################################
variable "role" {}

variable "consul_url" {}

variable "server_count" {}

variable "client_count" {}

variable "cost_center_id" {}
variable "base_hostname" {}
variable "domain" {}
variable "num_cpus" {}
variable "memory" {}
variable "resource_pool_id" {}

variable "datastore_list" {
  default = []
}

variable "guest_id" {}
variable "scsi_type" {}
variable "network_id" {}
variable "network_adapter_type" {}
variable "disk_size" {}
variable "disk_eagerly_scrub" {}
variable "disk_thin_provisioned" {}
variable "template_uuid" {}
variable "ipv4_subnet" {}
variable "ipv4_host" {}
variable "ipv4_netmask" {}
variable "ipv4_gateway" {}

variable "ipv4_dns" {
  default = []
}

variable "ssh_username" {}
variable "ssh_password" {}
variable "timeout" {}

variable "wait_on" {
  default = []
}

###############################################################################
# Force Inter-Module Dependency
###############################################################################
resource "null_resource" "waited_on" {
  count = "${length(var.wait_on)}"

  provisioner "local-exec" {
    command = "echo Dependency Resolved: ${var.role} depends upon ${element(var.wait_on, count.index)}"
  }
}

###############################################################################
# Provision Servers
###############################################################################
resource "vsphere_virtual_machine" "build_server" {
  count    = "${var.server_count + var.client_count}"
  tags     = ["${var.cost_center_id}"]
  name     = "${format("${var.base_hostname}-%02d", count.index+1)}"
  num_cpus = "${var.num_cpus}"
  memory   = "${var.memory}"

  resource_pool_id = "${var.resource_pool_id}"
  datastore_id     = "${element(var.datastore_list, count.index % length(var.datastore_list))}"
  guest_id         = "${var.guest_id}"
  scsi_type        = "${var.scsi_type}"

  network_interface {
    network_id   = "${var.network_id}"
    adapter_type = "${var.network_adapter_type}"
  }

  disk {
    label            = "${format("${var.base_hostname}-%02d", count.index+1)}.vmdk"
    size             = "${var.disk_size}"
    eagerly_scrub    = "${var.disk_eagerly_scrub}"
    thin_provisioned = "${var.disk_thin_provisioned}"
  }

  clone {
    template_uuid = "${var.template_uuid}"
    timeout       = "${var.timeout}"

    customize {
      linux_options {
        host_name = "${format("${var.base_hostname}-%02d", count.index+1)}"
        domain    = "${var.domain}"
      }

      network_interface {
        ipv4_address = "${var.ipv4_subnet}.${var.ipv4_host + count.index + 1}"
        ipv4_netmask = "${var.ipv4_netmask}"
      }

      ipv4_gateway    = "${var.ipv4_gateway}"
      dns_server_list = "${var.ipv4_dns}"
    }
  }

  depends_on = [
    "null_resource.waited_on",
  ]
}

###############################################################################
# Prep Consul Nodes
###############################################################################
resource "null_resource" "prep_consul_nodes" {
  count = "${var.server_count + var.client_count}"

  connection {
    user     = "${var.ssh_username}"
    password = "${var.ssh_password}"
    host     = "${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, count.index)}"
    type     = "ssh"
  }

  provisioner "file" {
    source      = "${path.module}/config/consul-template.service"
    destination = "/tmp/consul.service"
  }

  provisioner "remote-exec" {
    inline = [
      "set -eou pipefail",
      "rm -f /tmp/consul.zip && rm -f /tmp/consul && sudo rm -f /usr/bin/consul || true",
      "sudo yum install -y unzip",
      "curl -o /tmp/consul.zip -L ${var.consul_url}",
      "unzip /tmp/consul.zip -d /tmp && rm /tmp/consul.zip",
      "sudo mv /tmp/consul /usr/bin/.",
      "sudo useradd -s /bin/bash -p '$1$aAhgTeJd$VH0HkGpEF/D1Hhm/ZVkSd.' -U consul",
      "sudo mkdir -p /opt/consul/data",
      "sudo chown consul:consul /opt/consul/data",
      "sudo mkdir -p /opt/consul/config",
      "sudo chown consul:consul /opt/consul/config"
    ]
  }
}

###############################################################################
# Launch Consul Server Node Agents
###############################################################################
resource "null_resource" "launch_consul_server_node_agent" {
  count = "${var.server_count}"

  connection {
    user     = "${var.ssh_username}"
    password = "${var.ssh_password}"
    host     = "${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, count.index)}"
    type     = "ssh"
  }

  provisioner "file" {
    source      = "${path.module}/config/consul-template.service"
    destination = "/tmp/consul.service"
  }

  provisioner "remote-exec" {
    inline = [
      "set -eou pipefail",
      "sed -i \"s/ExecStart=/ExecStart=\\/usr\\/bin\\/consul agent -syslog -data-dir \\/opt\\/consul\\/data -config-dir \\/opt\\/consul\\/config -bind=${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, count.index)} -server -bootstrap-expect=${var.server_count} -ui -client 0.0.0.0/g\" /tmp/consul.service",
      "sudo mv /tmp/consul.service /usr/lib/systemd/system/consul.service",
      "sudo systemctl start consul.service",
      "sudo systemctl enable consul"
    ]
  }

  depends_on = [
    "null_resource.prep_consul_nodes",
  ]
}

###############################################################################
# Bootstrap Consul Server Cluster
###############################################################################
resource "null_resource" "bootstrap_consul_server_cluster" {
  connection {
    user     = "${var.ssh_username}"
    password = "${var.ssh_password}"
    host     = "${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, 0)}"
    type     = "ssh"
  }

  provisioner "remote-exec" {
    inline = [
      "set -eou pipefail",
      "consul join ${join(" ", slice(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, 0, var.server_count))}"
    ]
  }

  depends_on = [
    "null_resource.launch_consul_server_node_agent",
  ]
}

###############################################################################
# Launch Consul Client Node Agents
###############################################################################
resource "null_resource" "launch_consul_client_node_agent" {
  count = "${var.client_count}"

  connection {
    user     = "${var.ssh_username}"
    password = "${var.ssh_password}"
    host     = "${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, count.index + var.server_count)}"
    type     = "ssh"
  }

  provisioner "file" {
    source      = "${path.module}/config/consul-template.service"
    destination = "/tmp/consul.service"
  }

  provisioner "remote-exec" {
    inline = [
      "set -eou pipefail",
      "sed -i \"s/ExecStart=/ExecStart=\\/usr\\/bin\\/consul agent -syslog -data-dir \\/opt\\/consul\\/data -config-dir \\/opt\\/consul\\/config -bind=${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, count.index + var.server_count)} -retry-join ${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, 0)}/g\" /tmp/consul.service",
      "sudo mv /tmp/consul.service /usr/lib/systemd/system/consul.service",
      "sudo systemctl start consul.service",
      "sudo systemctl enable consul"
    ]
  }

  depends_on = [
    "null_resource.prep_consul_nodes"
  ]
}

###############################################################################
# Join Consul Client Nodes to Cluster
###############################################################################
# resource "null_resource" "join_consul_client_node_to_cluster" {
#   count = "${var.client_count}"

#   connection {
#     user     = "${var.ssh_username}"
#     password = "${var.ssh_password}"
#     host     = "${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, count.index + var.server_count)}"
#     type     = "ssh"
#   }

#   provisioner "remote-exec" {
#     inline = [
#       "set -eou pipefail",
#       "sudo consul join ${element(vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address, 0)}"
#     ]
#   }

#   depends_on = [
#     "null_resource.bootstrap_consul_server_cluster",
#     "null_resource.launch_consul_client_node_agent",
#   ]
# }

###############################################################################
# Outputs
###############################################################################
output "ipv4_max_host" {
  value = "${var.ipv4_host + var.server_count + var.client_count}"
}

output "ipv4_address_list" {
  value = "${vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address}"
}

output "ipv4_host_list" {
  value = "${split(",", replace(join(",", vsphere_virtual_machine.build_server.*.clone.0.customize.0.network_interface.0.ipv4_address), "${var.ipv4_subnet}.", ""))}"
}

output "hostname_list" {
  value = "${vsphere_virtual_machine.build_server.*.name}"
}

output "wait_on" {
  value      = "Servers Successfully Provisioned: ${var.role}"
  depends_on = ["null_resource.launch_consul_client_node_agent"]
}
