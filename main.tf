###############################################################################
# Provider Configuration
###############################################################################
provider "vsphere" {
  user           = "${var.vsphere_user}"
  password       = "${var.vsphere_password}"
  vsphere_server = "${var.vsphere_server}"

  allow_unverified_ssl = true
}

###############################################################################
# State Storage
###############################################################################
terraform {
  backend "s3" {}
}

###############################################################################
# Retrieve Information from VSphere for Use in Build and Configuration
###############################################################################
module "initialize_datasources" {
  source = "./initialize_datasources"

  datacenter_name             = "${var.datacenter}"
  resource_pool_name          = "${var.resource_pool}"
  datastore_name1             = "${var.datastore_name1}"
  datastore_name2             = "${var.datastore_name2}"
  network_name                = "${var.network}"
  template_name               = "${var.template}"
  cost_center_tag_name        = "${var.cost_center_tag_name}"
  cost_center_tag_description = "${var.cost_center_tag_description}"
  cost_center_name            = "${var.cost_center_name}"
  cost_center_description     = "${var.cost_center_description}"
}

###############################################################################
# Provision Consul Cluster
###############################################################################
module "provision_consul_cluster" {
  source = "./provision_consul_cluster"

  role                     = "consul"
  consul_url               = "${var.consul_url}"
  server_count             = "${var.consul_server_count}"
  client_count             = "${var.consul_client_count}"
  cost_center_id           = "${module.initialize_datasources.cost_center_id}"
  base_hostname            = "${var.consul_base_hostname}"
  domain                   = "${var.domain}"
  num_cpus                 = "${var.consul_num_cpus}"
  memory                   = "${var.consul_memory}"
  resource_pool_id         = "${module.initialize_datasources.resource_pool_id}"
  datastore_list           = "${list(module.initialize_datasources.datastore_id2, module.initialize_datasources.datastore_id1)}"
  guest_id                 = "${module.initialize_datasources.guest_id}"
  scsi_type                = "${module.initialize_datasources.scsi_type}"
  network_id               = "${module.initialize_datasources.network_id}"
  network_adapter_type     = "${module.initialize_datasources.network_adapter_type}"
  disk_size                = "${module.initialize_datasources.disk_size}"
  disk_eagerly_scrub       = "${module.initialize_datasources.disk_eagerly_scrub}"
  disk_thin_provisioned    = "${module.initialize_datasources.disk_thin_provisioned}"
  template_uuid            = "${module.initialize_datasources.template_uuid}"
  ipv4_subnet              = "${var.ipv4_subnet}"
  ipv4_host                = "${var.ipv4_max_host}"
  ipv4_netmask             = "${var.ipv4_netmask}"
  ipv4_gateway             = "${var.ipv4_gateway}"
  ipv4_dns                 = "${var.ipv4_dns}"
  ssh_username             = "${var.ssh_username}"
  ssh_password             = "${var.ssh_password}"
  timeout                  = "${var.consul_timeout}"
}
